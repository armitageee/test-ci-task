![Alt text](assets/screen.png)

Не совсем понимаю, как реализовать данное условие до конца, у меня не очень богатый опыт в конвертации питонячих пакетов в deb или rpm,
джоба вызывает мейкфайл в котором указаны все условия для сборки deb пакета(метаданные можно изменять в темплейтах, с изменением путей конечного bin файла до конца не разобрался), после чего запускается проверка работоспособности установленного на раннере пакета.
В данном примере был взят пакет xurl (описание функционала ниже)

### xurl

extract links (href data) from html files/web pages.

#### Installation
```
pip install xurl
```

#### Options
run the `xurl -h` or `xurl --help` for options
```
-a = append an URL to start of the links
-c = contain text (REGEX)
-C = not contain text (REGEX)
-q = quiet mode (do not print Errors/Warnings/Infos)
-v = version
```

#### Usages
```
xurl https://example.com
```
and same for the files
```
xurl path/to/file
```
search using regex
```
xurl https://example.com -c "section\-[1-10].*.[pdf|xlsx]"
```
пример работы джобы, пакет ставится и работает


![Alt text](assets/screen2.png)
