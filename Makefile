# Название вашего пакета и версия
PACKAGE_NAME = xurl
VERSION = 0.0.5

# Цель по умолчанию
all: build

# Цель для сборки DEB пакета
build:
	cd $(PACKAGE_NAME)-$(VERSION) && \
	python3 setup.py --command-packages=stdeb.command sdist_dsc && \
	cd deb_dist/$(PACKAGE_NAME)-$(VERSION) && \
	dpkg-buildpackage -us -uc -rfakeroot

# Цель для очистки рабочего каталога
clean:
	rm -rf $(PACKAGE_NAME)-$(VERSION)/deb_dist
	rm -rf $(PACKAGE_NAME)-$(VERSION)/$(PACKAGE_NAME).egg-info

